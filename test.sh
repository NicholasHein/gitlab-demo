#!/bin/bash
set -e
set -x

cd "`dirname $0`"

echo "Testing integrity..."

gdb "$@" -batch -return-child-result -x ./test.gdb -cd=. demo

echo "Testing output..."

diff <(./demo) <(echo 'Hello, world!')
