#!/usr/bin/make -f

FAILARGS=-DFAIL
NFAILARGS=

ifeq ($(FAIL),1)
	ARGS=$(FAILARGS)
else
	ARGS=$(NFAILARGS)
endif

demo: demo.c
	gcc demo.c -o demo -g $(ARGS)

.PHONY: clean
clean:
	rm -rf ./demo
