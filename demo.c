#include <stdio.h>
#include <stdlib.h>

int main (void) {
    #ifndef FAIL
    printf("Hello, world!\n");
    #else
    printf("Goodbye, world!\n");
    abort();
    #endif
    return 0;
}
